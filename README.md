# Final project for Demo shop application testing
Brief presentation of my project:
## Short Description
This application is responsible for testing the main functionality of the Demo Shop App
- Login
- Add to wishlist
- Checkout
- Product Listing
- Search
- Cart
- Add to wishlist
# Tech Stack Used
- Java 17.05
- Maven
- Selenide Framework
- PageObject Models
#How to run the tests
- git clone https://gitlab.com/Ionela1/ionela-first-project.git
- Execute the following commands to execute tests:
#### Execute all tests
- mvn clean test  
#### Generate report
- mvn allure:report 
> Open and present report
- mvn allure:serve 
####Page Objects
- Page
- MainPage
- Header
- Footer
- Modal
- ProductDetailsPage
- WishListPage
- Product
- ProductExpectedResults
- Account
- DemoShopApp
- ####Tests implemented:

| Test name                                                          | Execution status |        Date |
|--------------------------------------------------------------------|:----------------:|------------:|
| Test DemoShop App Title                                            |      Passed      |  01.05.2023 |
| Test DemoShop Footer BuildDate Details                             |      Passed      |  01.05.2023 |
| Test DemoShop Footer Contains ResetIcon                            |      Passed      |  01.05.2023 |
| Test DemoShop Footer Question Icon                                 |      Passed      |  01.05.2023 |
| Test user can login on the demo app                                |      Passed      |  01.05.2023 |
| Test user can't login on page without password                     |      Passed      |  01.05.2023 |
| Test modal components are displayed and modal can be closed        |      Passed      |  01.05.2023 |
| Test user can click on a product                                   |      Passed      |  01.05.2023 |
| Test user can add product to wishlist                              |      Passed      |  01.05.2023 |
| Test user can add products one by one to basket                    |      Passed      |  01.05.2023 |
| Test number of products from favorites is decreased after deletion |      Passed      |  01.05.2023 | 
| Test added product to favorites can be deleted                     |      Passed      |  01.05.2023 |
| Test added product to basket can be deleted                        |      Passed      |  01.05.2023 |
| Test added product to basket is in cart                            |      Passed      |  01.05.2023 |
| Test added product to basket can be increased in cart              |      Passed      |  01.05.2023 |
| Test added product to basket can be decreased in cart              |      Passed      |  01.05.2023 |
| Test added product to basket has correct price                     |    >>Failed<<    |  01.05.2023 |
| Test search product that does not exist pressing enter key         |      Passed      |  01.05.2023 | 
| Test search product with pressing key                              |    >>Failed<<    |  01.05.2023 |
____________________________________________________________________________________________<br />
Reporting is also available in the Allure-results folder
-tests are executed