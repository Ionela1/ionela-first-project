package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement details = $(".nav-link");
    private final SelenideElement questionIcon = $("[data-icon=question]");
    private final SelenideElement resetIconTitle = $("[data-icon=undo]");

    public String getDetails() {
        return this.details.getText();
    }

    public SelenideElement getQuestionIcon() {
        return questionIcon;
    }

    public SelenideElement getResetIconTitle() {
        return resetIconTitle;
    }
}
