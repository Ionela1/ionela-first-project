package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn.btn-primary");

    public Modal() {
    }

    public SelenideElement getUsername() {
        return this.username;
    }

    public SelenideElement getPassword() {
        return this.password;
    }

    public String getModalTitle() {
        return modalTitle.text();
    }

    public void validateModalComponents() {
        System.out.println("6. Verify that the Login Button is displayed");
        System.out.println("7. Verify that the Login Button is enabled");
    }

    /**
     * Clicks
     */
    public void clickOnCloseButton() {
        System.out.println("Clicked on the 'x' button");
        this.closeButton.click();
        sleep(150);
    }

    public void clickOnUsernameField() {
        this.username.click();
    }

    /**
     * Typing actions
     */

    public void typeInUsernameField(String userTOType) {
        System.out.println("Typed in username: " + userTOType);
        this.username.click();
        this.username.sendKeys(userTOType);
    }

    public void typeInPasswordField(String passwordToType) {
        System.out.println("Typed in password: " + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    public void clickOnTheLoginButton() {

        this.loginButton.click();
        sleep(150);
    }

    public void clickOnPasswordField() {
        this.password.click();
    }

    /**
     * *Validators
     *
     * @return
     */
    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUsernameFieldIsDisplayed() {
        return this.username.exists() && this.username.isDisplayed();
    }

    public boolean validatePasswordFieldIsDisplayed() {
        return this.password.exists() && this.password.isDisplayed();
    }

    public boolean validateLoginButtonExists() {
        return this.loginButton.exists();
    }

    public boolean validateLoginButtonIsDisplayed() {
        return this.loginButton.isDisplayed();
    }
}
