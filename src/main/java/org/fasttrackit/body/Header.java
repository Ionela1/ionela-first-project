package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement logoIconUrl = $(".fa-shopping-bag");
    private final String shoppingCartIconUrl;
    private final String wishListIconUrl;
    private final String greetingsMessage;
    private final SelenideElement signInButton = $(".fa-sign-in-alt");
    private final SelenideElement signOutButton = $(".fa-sign-out-alt");

    public Header() {
        this.shoppingCartIconUrl = "/cart";
        this.wishListIconUrl = "#/wishlist";
        this.greetingsMessage = "Hello guest!";
    }

    public Header(String user) {
        this.shoppingCartIconUrl = "/cart";
        this.wishListIconUrl = "#/wishlist";
        this.greetingsMessage = "Hi " + user + "!";
    }

    /*
     * Getters
     */
    public SelenideElement getLogoIconUrl() {
        return this.logoIconUrl;
    }

    public String getShoppingCartIconUrl() {
        return this.shoppingCartIconUrl;
    }

    public String getWishListIconUrl() {
        return this.wishListIconUrl;
    }

    public String getGreetingsMessage() {
        return this.greetingsMessage;
    }

    public SelenideElement getSignInButton() {
        return this.signInButton;
    }

    /*
     * Actions
     */
    public void clickOnTheLoginButton() {
        System.out.println("Clicked On the Login button.");
        this.signInButton.click();
    }

    public void clickOnTheLogOutButton() {
        this.signOutButton.click();
    }

    public void clickOnTheWishListIconUrl() {
        System.out.println("Clicked on the wish list button.");

    }

    public void clickOnTheLogoButton() {
        this.logoIconUrl.click();
    }
}
