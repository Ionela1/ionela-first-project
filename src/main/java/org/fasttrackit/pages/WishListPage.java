package org.fasttrackit.pages;

public class WishListPage extends MainPage {
    private final String pageSubTitle;

    public WishListPage() {
        this.pageSubTitle = "Wishlist";
        System.out.println("Wishlist page was created!");
    }

    public void validateThatTheWishListPageIsDisplayed() {
        System.out.println(" Verify that wishlist page is displayed");
    }
}
