package org.fasttrackit.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;
import org.fasttrackit.products.Product;


import javax.lang.model.element.Element;
import java.sql.SQLOutput;
import java.util.Objects;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;


public class MainPage extends Page {
    private final String title = Selenide.title();
    private Header header;
    private final Footer footer;

    private final SelenideElement modal = $(".modal-dialog");

    public MainPage() {
        System.out.println("Constructing header");
        this.header = new Header();
        System.out.println("Constructing footer");
        this.footer = new Footer();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return this.header;
    }

    public Footer getFooter() {
        return footer;
    }

    public String verifyThatTitleIsDisplayedOnScreen() {
        return title;
    }

    public void validateThatFooterContainsAllElements() {
        System.out.println("1. Verify footer details are: " + footer.getDetails());
        System.out.println("2. Verify footer has Question Icon: " + footer.getQuestionIcon());
        System.out.println("3. Verify footer has Reset Icon title: " + footer.getResetIconTitle());
    }

    public void validateThatHeaderContainsAllElements() {
        System.out.println("1. Verify that logo url is: " + header.getLogoIconUrl());
        System.out.println("2. Verify that shopping cart url is: " + header.getShoppingCartIconUrl());
        System.out.println("3. Verify that wish list url is: " + header.getWishListIconUrl());
        System.out.println("4. Verify that welcome message is: " + header.getGreetingsMessage());
        System.out.println("5. Verify that sign-in is: " + header.getSignInButton());
    }

    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that the modal is displayed on page");
        return this.modal.exists() && this.modal.isDisplayed();
    }

    public boolean validateModalIsNotDisplayed() {
        System.out.println("1. Verify that modal is not on page");
        return !modal.exists();
    }

    public void clickOnTheLoginButton() {

        this.header.clickOnTheLoginButton();
    }

    public void clickOnTheWishListButton() {
        System.out.println("--------");
        this.header.clickOnTheWishListIconUrl();
    }

    public void clickOnTheLogoButton() {
        System.out.println("--------");
        this.header.clickOnTheLogoButton();
    }

    public void logUserOut() {
        this.header.clickOnTheLogOutButton();
        sleep(150);
    }

    public void returnToHomePage() {
        this.header.getLogoIconUrl().click();
    }

    public boolean isProductDisplayedOnHomePage(Product p) {
        open("https://fasttrackit-test.netlify.app/#");
        $("#input-search").setValue(p.getExpectedResults().getTitle()).pressEnter();
        sleep(2 * 1000);

        return $(".container").is(exactText(p.getExpectedResults().getTitle()));
    }


    public boolean isProductDisplayedOnHomePageAFterPreesingSearchButton(Product p) {
        open("https://fasttrackit-test.netlify.app/#");
        searchForProduct(p.getExpectedResults().getTitle());
        ElementsCollection all = $$("div>.card-link");
        System.out.println("elements"+all);
        int size =0;
        /*if (size>0)
            return true;
        return false;*/
        return all.filter(Condition.attribute("a", p.getExpectedResults().getTitle())).size() > 0;
    }
    private void searchForProduct(String productTitle) {
        $("#input-search").setValue(productTitle);
        $(".btn.btn-light.btn-sm").pressEnter();
        sleep(2 * 1000);
    }
    public boolean isProductDisplayedOnHomePageAFterPreesingSearchButton1(Product p) {
        open("https://fasttrackit-test.netlify.app/#");
        searchForProduct(p.getExpectedResults().getTitle());
        ElementsCollection elements = $$(".text-center.card-body");
        return elements.filter(Condition.attribute("div>.card-link", p.getExpectedResults().getTitle())).size() > 0;
    }
    public boolean isProductDisplayedOnHomePageAFterPreesingSearchButton3(Product p) {
        open("https://fasttrackit-test.netlify.app/#");
        $("#input-search").setValue(p.getExpectedResults().getTitle());
        $(".btn.btn-light.btn-sm").pressEnter();
        sleep(2 * 1000);
        ElementsCollection selem=  $$(".text-center.card-body");
        boolean isProductDisplayedOnHomePageAFterPreesingSearchButton = false;
        for (SelenideElement element : selem) {
            if(element.text().toLowerCase().contains(p.getExpectedResults().getTitle() )){

                //System.out.println();
                return true;}
        }
        return false;

    }
}
