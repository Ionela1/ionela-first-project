package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishListPage;

import static com.codeborne.selenide.Selenide.sleep;

public class DemoShopApp {
    private static final String APP_TITLE = "Demo Shop Testing simulator = -";
    public static final String DEMO_SHOP_TITLE = "Demo shop";

    public static void main(String[] args) {
        System.out.println(APP_TITLE);
        MainPage homePage = new MainPage();

        verifyStaticPage(homePage);
        verifyLoginModal(homePage);
        verifyWishListPage(homePage);

        String beetleUser = "beetle";
        loginWithUser(homePage, beetleUser);
        validateLoggedInUser(homePage, beetleUser);

        sleep(2 * 1000);
    }

    private static void validateLoggedInUser(MainPage homePage, String beetleUser) {
        Header loggedInHeader = new Header(beetleUser);
        homePage.setHeader(loggedInHeader);
        homePage.validateThatHeaderContainsAllElements();
    }

    private static void loginWithUser(MainPage homePage, String beetleUser) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnUsernameField();
        modal.typeInUsernameField(beetleUser);
        modal.clickOnPasswordField();
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
    }

    private static void verifyWishListPage(MainPage homePage) {
        homePage.clickOnTheWishListButton();
        WishListPage wishListPage = new WishListPage();
        wishListPage.validateThatTheWishListPageIsDisplayed();
        wishListPage.clickOnTheLogoButton();
    }

    private static void verifyLoginModal(MainPage homePage) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        homePage.validateModalIsNotDisplayed();
    }

    private static void verifyStaticPage(MainPage homePage) {
        homePage.verifyThatTitleIsDisplayedOnScreen();
        homePage.validateThatFooterContainsAllElements();
        homePage.validateThatHeaderContainsAllElements();
    }
}