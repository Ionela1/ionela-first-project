package org.fasttrackit.cart;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.products.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Objects;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.Integer.parseInt;


public class Cart {
    private Integer cartNoOfProducts;
    private Integer favoritesNoOfProducts;
    private String userOfTheCart;
    private SelenideElement cartProducts;

    public Cart() {

        if (($("[href='#/cart']>span.fa-layers-counter.shopping_cart_badge") != null) && $("[href='#/cart']>span.fa-layers-counter.shopping_cart_badge").exists()) {
            this.cartNoOfProducts = parseInt($("[href='#/cart']>span.fa-layers-counter.shopping_cart_badge").getValue());
        } else {
            this.cartNoOfProducts = 0;
        }

        if (($("[href='#/wishlist']>span.fa-layers-counter.shopping_cart_badge") != null) && $("[href='#/wishlist']>span.fa-layers-counter.shopping_cart_badge").exists()) {
            this.favoritesNoOfProducts = parseInt($("[href='#/wishlist']>span.fa-layers-counter.shopping_cart_badge").getValue());
        } else {
            this.favoritesNoOfProducts = 0;
        }
        //System.out.println("nr de produse este: "+this.cartNoOfProducts);
        //this.userOfTheCart = $("#user-name").toString();
        //System.out.println("Userul este:  " + this.userOfTheCart);
        WebElement webElement = $(".col").getWrappedElement();
        System.out.println("Userul este:  " + webElement);
        this.cartProducts = $(".col");

    }

    public Integer getCartNoOfProducts() {

        if (($("[href='#/cart']>span.fa-layers-counter.shopping_cart_badge") != null) &&
                $("[href='#/cart']>span.fa-layers-counter.shopping_cart_badge").exists()) {
            this.cartNoOfProducts = parseInt($("[href='#/cart']>span.fa-layers-counter.shopping_cart_badge").innerText());
        } else {
            this.cartNoOfProducts = 0;
        }
        return this.cartNoOfProducts;
    }

    public Integer getFavoritesNoOfProducts() {

        if (($("[href='#/wishlist']>span.fa-layers-counter.shopping_cart_badge") != null) &&
                $("[href='#/wishlist']>span.fa-layers-counter.shopping_cart_badge").exists()) {
            this.favoritesNoOfProducts = parseInt($("[href='#/wishlist']>span.fa-layers-counter.shopping_cart_badge").innerText());
        } else {
            this.favoritesNoOfProducts = 0;
        }
        return this.favoritesNoOfProducts;
    }

    public SelenideElement getProducts() {
        // if(this.products.isDisplayed())

        /*for (SelenideElement product : this.products) {
            System.out.println(product);
        }*/
        return this.cartProducts;
    }


    public void deleteProductFromCart(Product p) {
        open("https://fasttrackit-test.netlify.app/#/cart");
        SelenideElement foundProductByTitle = $$("div.container>div.row").find(text(p.getExpectedResults().getTitle()));
        int lengthPriceInformation = p.getExpectedResults().getPrice().length();
        String actualPrice = p.getExpectedResults().getPrice().substring(0, lengthPriceInformation - 3);
        SelenideElement foundProductByPrice = $$("div.container>div.row").find(text(actualPrice));
        SelenideElement foundProductInCart = foundProductByPrice;
        if (Objects.equals(foundProductByTitle, foundProductByPrice)) {
            foundProductInCart = foundProductByTitle;
        }
        SelenideElement buttonMinusOfFoundProductInCart = foundProductInCart.find(By.cssSelector(".btn.btn-link>[data-icon='minus-circle']")).parent();
        buttonMinusOfFoundProductInCart.click();
        sleep(2000);
    }

    public void deleteProductFromFavorites(Product p) {
        open("https://fasttrackit-test.netlify.app/#/wishlist");
        SelenideElement foundProductByTitle = $$("div.row.row-cols-xl-4.row-cols-lg-3.row-cols-md-2.row-cols-sm-2.row-cols-1>div.col").find(text(p.getExpectedResults().getTitle()));
        int lengthPriceInformation = p.getExpectedResults().getPrice().length();
        String actualPrice = p.getExpectedResults().getPrice().substring(0, lengthPriceInformation - 4);
        SelenideElement foundProductByPrice = $$("div.row.row-cols-xl-4.row-cols-lg-3.row-cols-md-2.row-cols-sm-2.row-cols-1>div.col").find(text(actualPrice));
        SelenideElement foundProductInCart = foundProductByPrice;
        if (Objects.equals(foundProductByTitle, foundProductByPrice)) {
            foundProductInCart = foundProductByTitle;
        }
        SelenideElement buttonMinusOfFoundProductInCart = foundProductInCart.find(By.cssSelector("[data-icon='heart-broken']"));
        buttonMinusOfFoundProductInCart.click();
        sleep(2000);
    }

    public SelenideElement findProductInCart(Product p) {
        open("https://fasttrackit-test.netlify.app/#/cart");
        SelenideElement foundProductInCart = $(".text-center.container");
        if (foundProductInCart.is(Condition.exactText("How about adding some products in your cart?"))) {
            return foundProductInCart;
        } else {
            SelenideElement foundProductByTitle = $$("div.container>div.row").find(text(p.getExpectedResults().getTitle()));
            System.out.println("element found is: " + foundProductByTitle);
            int lengthPriceInformation = p.getExpectedResults().getPrice().length();
            String actualPrice = p.getExpectedResults().getPrice().substring(0, lengthPriceInformation - 3);
            SelenideElement foundProductByPrice = $$("div.container>div.row").find(text(actualPrice));
            if (Objects.equals(foundProductByTitle, foundProductByPrice)) {
                foundProductInCart = foundProductByTitle;
            }
        }
        return foundProductInCart;
    }

    public SelenideElement findProductInFavorites(Product p) {
        open("https://fasttrackit-test.netlify.app/#/wishlist");
        SelenideElement foundProductInFavorites = $("div.row.row-cols-xl-4.row-cols-lg-3.row-cols-md-2.row-cols-sm-2.row-cols-1>div.col");
        System.out.println("sfsfdsd" + foundProductInFavorites);
        if (foundProductInFavorites.is(Condition.exactText(p.getExpectedResults().getTitle()))) {
            System.out.println("este favorites");
        }
        return foundProductInFavorites;

    }

    public void getProductsFromCart() {
        open("https://fasttrackit-test.netlify.app/#/cart");
        //SelenideElement actualCartProducts = $(".row");
    }

    public boolean cartIsEmpty() {
        open("https://fasttrackit-test.netlify.app/#/cart");
        if ($(".text-center.container").is(Condition.exactText("How about adding some products in your cart?"))) {
            return true;
        }
        return false;
    }

    public boolean favoritesIsEmpty() {
        open("https://fasttrackit-test.netlify.app/#/wishlist");
        if ($("div.row.row-cols-xl-4.row-cols-lg-3.row-cols-md-2.row-cols-sm-2.row-cols-1>div.col") != null) {
            return true;
        }
        return false;
    }
    public String getNoOfProducts(Product p){
        open("https://fasttrackit-test.netlify.app/#/cart");
        if (cartIsEmpty()) {
            return "0";
        }else{
        String currentNoOfProductsInCart = $("div.col-md-auto>div").innerText();
        return currentNoOfProductsInCart;}
    }

    public void increaseProductInCart(Product p) {
        SelenideElement elem = findProductInCart(p).find("[data-icon=plus-circle]");
        elem.click();
    }
    public void decreaseProductInCart(Product p) {
        SelenideElement elem = findProductInCart(p).find("[data-icon=minus-circle]");
        elem.click();
    }
}