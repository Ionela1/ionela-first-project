package org.fasttrackit.products;

public class ProductExpectedResults {
    private final String title;
    private final String price;

    public ProductExpectedResults(String title, String price) {
        this.title = title;
        this.price = price;
    }

    public String getTitle() {
        return this.title;
    }

    public String getPrice() {
        return this.price;
    }
}
