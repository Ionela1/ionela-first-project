package org.fasttrackit.products;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement productLink;
    private final SelenideElement card;
    private final SelenideElement addToBasketButton;
    private final SelenideElement addToFavorites;
    private final ProductExpectedResults expectedResults;

    public Product(String productId, ProductExpectedResults expectedResults) {
        this.productLink = $(String.format(".card-body [href='#/product/%s']", productId));
        this.expectedResults = expectedResults;
        this.card = productLink.parent().parent();
        this.addToBasketButton = card.$(".fa-cart-plus");
        this.addToFavorites = card.$(".fa-heart");
    }

    public void clickOnProduct() {
        this.productLink.click();
    }

    public void addProductToBasket() {
        this.addToBasketButton.click();
    }

    public void addProductToFavorites() {
        this.addToFavorites.scrollTo();
        this.addToFavorites.click();
    }

    public ProductExpectedResults getExpectedResults() {
        return this.expectedResults;
    }

    @Override
    public String toString() {
        return this.productLink.text();
    }
}
