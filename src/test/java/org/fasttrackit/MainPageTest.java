package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MainPageTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @BeforeMethod
    public void setup() {
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Search product with Enter key test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void search_product_with_pressing_key_test(Product p) {
        assertTrue(homePage.isProductDisplayedOnHomePage(p), "Expected searched product to be found: " + p.getExpectedResults().getTitle());
        sleep(2 * 1000);
    }
    @Test(testName = "Search product that does not exist with Enter key test"
    )
    public void search_product_that_does_not_exist_pressing_enter_key_test() {
        ProductExpectedResults productExpectedResults = new ProductExpectedResults("abcedf","123");
        Product p2= new Product("10000",productExpectedResults );
        assertFalse(homePage.isProductDisplayedOnHomePage(p2), "Expected searched product that does not exist to be not found: " + p2.getExpectedResults().getTitle());
        sleep(2 * 1000);
    }

    @Test(testName = "Search product with Enter key test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void search_product_pressing_button_test(Product p) {
        assertTrue(homePage.isProductDisplayedOnHomePageAFterPreesingSearchButton3(p), "Expected searched product to be found: " + p.getExpectedResults().getTitle());
        sleep(2 * 1000);
    }
    @Test(testName = "Search product that does not exist with Enter key test"
    )
    public void search_product_that_does_not_exist_pressing_test() {
        ProductExpectedResults productExpectedResults = new ProductExpectedResults("abcd","123");
        Product p2= new Product("10000",productExpectedResults );
        assertTrue(homePage.isProductDisplayedOnHomePageAFterPreesingSearchButton3(p2), "Expected searched product that does not exist to be not found: " + p2.getExpectedResults().getTitle());
        sleep(2 * 1000);
    }
}
