package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.cart.Cart;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class CartTest extends TestConfiguration {
    MainPage homePage = new MainPage();
    Cart actualCart = new Cart();

    @BeforeMethod
    public void setup() {
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Add product to basket test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void added_product_to_basket_is_in_cart_test(Product p) {
        p.addProductToBasket();
        assertTrue(actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getTitle()), "Expected product in the cart to be" + p.getExpectedResults().getTitle());
        if (actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getTitle()))
            actualCart.deleteProductFromCart(p);
        sleep(1000);
    }

    @Test(testName = "Price of added product to basket test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void added_product_to_basket_has_correct_price_test(Product p) {
        p.addProductToBasket();
        assertTrue(actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getPrice()), "Expected price for product in the cart to be" + p.getExpectedResults().getPrice());
        if (actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getPrice()))
            actualCart.deleteProductFromCart(p);
        sleep(1000);
    }

    @Test(testName = "Delete product from cart test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void __01_added_product_to_basket_can_be_deleted_test(Product p) {
        p.addProductToBasket();
        actualCart.deleteProductFromCart(p);
        assertFalse(actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getTitle()), "Expected product in the cart to be deleted" + p.getExpectedResults().getTitle());
        if (actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getPrice()))
            actualCart.deleteProductFromCart(p);
        sleep(1000);
    }

    @Test(testName = "Delete product from favorites test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void added_product_to_favorites_can_be_deleted_test(Product p) {
        p.addProductToFavorites();
        actualCart.deleteProductFromFavorites(p);
        assertFalse(actualCart.findProductInFavorites(p).toString().contains(p.getExpectedResults().getTitle()), "Expected deleted product to be found in favorites after deletion to be: " + actualCart.findProductInFavorites(p).toString().contains(p.getExpectedResults().getTitle()));
        sleep(2 * 1000);
    }

    @Test(testName = "Increase product from cart test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void __02_product_can_be_increased_in_cart_test(Product p) {
        p.addProductToBasket();
        int currentNoOfItemsOfACertainProduct = Integer.parseInt(actualCart.getNoOfProducts(p));
        actualCart.increaseProductInCart(p);
        currentNoOfItemsOfACertainProduct++;
        int currentNoOfItemsOfACertainProductAfterIncrease = Integer.parseInt(actualCart.getNoOfProducts(p));
        assertEquals(currentNoOfItemsOfACertainProduct,currentNoOfItemsOfACertainProductAfterIncrease, "Expected deleted product to be found in favorites after deletion to be: " + actualCart.findProductInFavorites(p).toString().contains(p.getExpectedResults().getTitle()));
        if (actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getPrice()))
            actualCart.deleteProductFromCart(p);
        sleep(2 * 1000);
    }
    @Test(testName = "Decrease product from cart test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void __03_product_can_be_decreased_in_cart_test(Product p) {
        p.addProductToBasket();
        int currentNoOfItemsOfACertainProduct = Integer.parseInt(actualCart.getNoOfProducts(p));
        actualCart.decreaseProductInCart(p);
        currentNoOfItemsOfACertainProduct--;
        int currentNoOfItemsOfACertainProductAfterDecrease = Integer.parseInt(actualCart.getNoOfProducts(p));
        assertEquals(currentNoOfItemsOfACertainProduct,currentNoOfItemsOfACertainProductAfterDecrease, "Expected deleted product to be found in favorites after deletion to be: " + currentNoOfItemsOfACertainProductAfterDecrease);
        if (actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getPrice()))
            actualCart.deleteProductFromCart(p);
        sleep(2 * 1000);
    }
}
