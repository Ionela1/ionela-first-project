package org.fasttrackit;

import org.fasttrackit.cart.Cart;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;

public class ProductTest extends TestConfiguration {
    MainPage homePage = new MainPage();
    Cart actualCart = new Cart();

    @BeforeMethod
    public void setup() {
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Click on product test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void user_can_click_on_a_product(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedTitle = p.getExpectedResults().getTitle();

        assertEquals(productPage.getTitle(), expectedTitle, "Expected title to be " + expectedTitle);
        sleep(2 * 1000);
    }

    @Test(testName = "Add product to basket test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void user_can_add_products_one_by_one_to_basket_test(Product p) {
        Integer cartNoOfProductsBeforeAdd = actualCart.getCartNoOfProducts();
        homePage.clickOnTheLogoButton();
        p.addProductToBasket();
        cartNoOfProductsBeforeAdd = cartNoOfProductsBeforeAdd + 1;
        assertEquals(cartNoOfProductsBeforeAdd, actualCart.getCartNoOfProducts(), "Expected number of products in the cart to be: " + actualCart.getCartNoOfProducts());
        if (actualCart.findProductInCart(p).toString().contains(p.getExpectedResults().getTitle()))
            actualCart.deleteProductFromCart(p);
        sleep(5000);
    }

    @Test(testName = "Add product to wishlist test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void user_can_add_product_to_wishlist_test(Product p) {
        Integer favoritesNoOfProductsBeforeAdd = actualCart.getFavoritesNoOfProducts();
        p.addProductToFavorites();
        favoritesNoOfProductsBeforeAdd = favoritesNoOfProductsBeforeAdd + 1;
        Integer favoritesNoOfProductsAfterAdd = actualCart.getFavoritesNoOfProducts();
        assertEquals(favoritesNoOfProductsBeforeAdd, actualCart.getFavoritesNoOfProducts(), "Expected number of products in the favorites to be: " + favoritesNoOfProductsAfterAdd);
        if (actualCart.findProductInFavorites(p).toString().contains(p.getExpectedResults().getTitle())) {
            actualCart.deleteProductFromFavorites(p);
        }
        sleep(2 * 1000);
    }

    @Test(testName = "Number of added products to favorites is decreased after deletion test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void number_of_products_from_favorites_is_decreased_after_deletion_test(Product p) {
        Integer favoritesNoOfProductsBeforeAdd = actualCart.getFavoritesNoOfProducts();
        p.addProductToFavorites();
        favoritesNoOfProductsBeforeAdd = favoritesNoOfProductsBeforeAdd + 1;
        Integer favoritesNoOfProductsAfterAdd = actualCart.getFavoritesNoOfProducts();
        assertEquals(favoritesNoOfProductsBeforeAdd, actualCart.getFavoritesNoOfProducts(), "Expected number of products in the favorites to be: " + favoritesNoOfProductsAfterAdd);
        if (actualCart.findProductInFavorites(p).toString().contains(p.getExpectedResults().getTitle())) {
            actualCart.deleteProductFromFavorites(p);
        }
        sleep(2 * 1000);
    }
}
