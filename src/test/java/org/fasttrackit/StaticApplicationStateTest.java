package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class StaticApplicationStateTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @Test
    public void verifyDemoShopAppTitle() {
        String appTitle = homePage.verifyThatTitleIsDisplayedOnScreen();
        assertEquals(appTitle, "Demo shop", "Application title is expected to be Demo shop.");
    }

    @Test
    public void verifyDemoShopFooterBuildDateDetails() {
        String footerDetails = homePage.getFooter().getDetails();
        assertEquals(footerDetails, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
    }

    @Test
    public void verifyDemoShopFooterQuestionIcon() {
        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(), "Expected icon to exist on Page");
        assertTrue(questionIcon.isDisplayed(), "Expected icon to be displayed on Page");
    }

    @Test
    public void verifyDemoShopFooterContainsResetIcon() {
        SelenideElement resetIconTitle = homePage.getFooter().getResetIconTitle();
        assertTrue(resetIconTitle.exists(), "Expected Reset Icon to exist on Page");
        assertTrue(resetIconTitle.isDisplayed(), "Expected Reset Icon is displayed");
    }
}
