package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Feature("User Login")
public class LoginTest extends TestConfiguration {
    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("I will cleanup after each test method. !!");
    }

    @AfterTest
    public void tearDownClass() {
        System.out.println("I will cleanup after each test class. !!");
    }

    @Test
    public void modal_components_are_displayed_and_modal_can_be_closed_test() {
        homePage.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal is displayed");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected modal title to be Login");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected close button to be displayed");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected username field to be displayed");
        assertTrue(modal.validatePasswordFieldIsDisplayed(), "Expected password field to be displayed");
        assertTrue(modal.validateLoginButtonExists(), "Expected login button to exist");
        assertTrue(modal.validateLoginButtonIsDisplayed(), "Expected login button to be displayed");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed!");
    }

    @Test(testName = "Login Test with valid user1:",
            dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class)
    public void user_can_login_on_the_demo_app(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        sleep(2000);
        assertFalse(homePage.validateModalIsDisplayed(), "Expected modal to be closed!");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Expected greetings message to contain the account user.");
        homePage.logUserOut();
        sleep(2000);
    }

    @Test(testName = "Login Test",
            description = "This test is responsible for testing that the user can't login on page without password",
            dependsOnMethods = {"modal_components_are_displayed_and_modal_can_be_closed_test", "user_can_login_on_the_demo_app"}
    )
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_page_without_password_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.clickOnTheLoginButton();
        assertFalse(homePage.validateModalIsNotDisplayed(), "Expected modal to remain on page!");
        sleep(3000);
        modal.clickOnCloseButton();
    }

    @Test
    public void user_cannot_login_with_locked_account() {
        Account lockedAccount = new Account("locked", "choochoo");
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(lockedAccount.getUsername());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain opened");
        sleep(3000);
        modal.clickOnCloseButton();
    }
}
